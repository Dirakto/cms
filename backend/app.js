'use-strict'
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const mongo = require('mongoose');
const router = express.Router();
const Laptop = require('../helpers/LaptopSchema');
// const bodyParser = require('body-parser')
const fs = require('fs');

let PORT = 4000;
var app = express();


app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

// app.use('/api', indexRouter);

app.use(router);

mongo.connect('mongodb://127.0.0.1:27017/cms-project', {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongo.connection;

db.once('open', function(){
  console.log("MongoDB connection established successfully");
});

app.listen(PORT, function(){
  console.log(`Server is listening on port: ${PORT}`);
});

// API routes

// mongo.set('debug', true);
router.post('/api/laptop', (req, res) => {
  findLaptopsByForm(res, req.body, Laptop);
});


async function findLaptopsByForm(res, form, requestData){
  let {
    prizeMin, prizeMax,
    category,
    wireless_card,
    optical_drive
  } = form;
  console.log(prizeMin+" "+prizeMax+" "+optical_drive+" "+category);
    try{
      let response = await Laptop.find({prize: { $gte: prizeMin, $lte: prizeMax}, opticalDrive: { $eq: optical_drive}})
      .sort( {[category]: 1 } )
      .exec()
      console.log(response);
      res.status(200).json(response)
      db
    }catch(err){
      res.status(503).send(err)
    }
}

router.get('/api/basic-form-data', (req, res) => {
  (async function(){
    let fullResponse = {categories: [], storageSizes: [], screenSizes: []};

    let response = await Categories.find({}).exec();
    response.map((element) => fullResponse.categories.push(element))

    response = await StorageSizes.find({}).exec()
    response.map((element) => fullResponse.storageSizes.push(element))

    response = await ScreenSizes.find({}).exec()
    response.map((element) => fullResponse.screenSizes.push(element))

    res.status(200).json(fullResponse);
  })()
});


router.get('/api/advanced-form-data', (req, res) => {
  (async function(){
    let fullResponse = {cpuProducers: [], cpuCores: [], baseSpeed: [], boostSpeed: [], memoryTypes: [], memorySizes: [], displayTypes: [],
                        horizontalResolutions: [], verticalResolutions: [], storageTypes: [], storageSizes: [], gpuNames: [], gpuMemorySizes: [], gpuMemoryTypes: []};

    let response = await CpuProducers.find({}).exec();
    response.map((element) => fullResponse.cpuProducers.push(element))

    response = await CpuCores.find({}).exec()
    response.map((element) => fullResponse.cpuCores.push(element))

    response = await BaseSpeed.find({}).exec()
    response.map((element) => fullResponse.baseSpeed.push(element))

    response = await BoostSpeed.find({}).exec()
    response.map((element) => fullResponse.boostSpeed.push(element))

    response = await MemoryRatings.find({}).exec()
    response.map((element) => fullResponse.memoryTypes.push(element))

    response = await MemorySizes.find({}).exec()
    response.map((element) => fullResponse.memorySizes.push(element))

    response = await DisplayRatings.find({}).exec()
    response.map((element) => fullResponse.displayTypes.push(element))

    response = await DisplayHorizontalResolutions.find({}).exec()
    response.map((element) => fullResponse.horizontalResolutions.push(element))

    response = await DisplayVerticalResolutions.find({}).exec()
    response.map((element) => fullResponse.verticalResolutions.push(element))

    response = await StorageRatings.find({}).exec()
    response.map((element) => fullResponse.storageTypes.push(element))

    response = await StorageSizes.find({}).exec()
    response.map((element) => fullResponse.storageSizes.push(element))

    response = await GpuProducers.find({}).exec()
    response.map((element) => fullResponse.gpuNames.push(element))

    response = await GpuSizes.find({}).exec()
    response.map((element) => fullResponse.gpuMemorySizes.push(element))

    response = await GpuTypes.find({}).exec()
    response.map((element) => fullResponse.gpuMemoryTypes.push(element))

    // response = await OpticalDrives.find({}).exec()
    // response.map((element) => fullResponse.opticalDrives.push(element))
    

    res.status(200).json(fullResponse);
  })()
});

const calculateResult = (value, prop) => {
  const type = value.slice(-1);
  if(type === '-'){
    return { [prop]: { $lt: parseInt(value.substring(0, value.length-1))}}
  }else if(type === '+'){
    return { [prop]: {$gt: parseInt(value.substring(0, value.length-1))}}
  }else if(type === '_'){
    let values = value.split('_');
    return { $and: [
      { [prop]: {$gte: parseInt(values[0])}},
      { [prop]: {$lte: parseInt(values[1])}}
    ]};
  }else{
    return { [prop]: { $eq: parseInt(value)}};
  }
};

const ObjectId = mongo.Types.ObjectId;

router.post('/api/laptops', ({body}, res) => {
  (async function(){

    let valueChecker = [];
    const a = body.advancedFormData;

    let find = {
      prize: { $lte: body.prize},
      
      // primaryStorageCap: calculateResult(body.formData.storageSize)
    };

    valueChecker.push(calculateResult(body.formData.screenSize, 'displaySize'));
    valueChecker.push(calculateResult(body.formData.storageSize, 'primaryStorageCap'));

    if(a.cpuProducer){
      find.cpuProducer = body.advancedFormData.cpuProducer;
    }
    if(a.cpuCores){
      find.cpuCores = a.cpuCores;
    }

    if(a.cpuBaseSpeed)
      valueChecker.push(calculateResult(a.cpuBaseSpeed, 'cpuBaseSpeed'));
    if(a.cpuBoostSpeed)
      valueChecker.push(calculateResult(a.cpuBoostSpeed, 'cpuBoostSpeed'));

    if(a.memoryTypes){
      find.memoryType = new ObjectId((await MemoryRatings.findOne({value: a.memoryTypes}).exec())._id);
    }
    
    if(a.memorySizes)
      valueChecker.push(calculateResult(a.memorySizes, 'memorySize'));
    
    if(a.horizontalResolution)
      valueChecker.push(calculateResult(a.horizontalResolution, 'horizontalResolution'));

    if(a.verticalResolution)
      valueChecker.push(calculateResult(a.verticalResolution, 'verticalResolution'));

    if(a.displayType)
      find.displayType = new ObjectId((await DisplayRatings.findOne({value: a.displayType}).exec())._id);

    if(a.storageType)
      find.primaryStorageModel = new ObjectId((await StorageRatings.findOne({value: a.storageType}).exec())._id);

    if(a.secondaryStorageType)
      find.secondaryStorageModel = new ObjectId((await StorageRatings.findOne({value: a.secondaryStorageType}).exec())._id);

    if(a.secondaryStorageSize)
      valueChecker.push(calculateResult(a.secondaryStorageSize, 'secondaryStorageCap'));
    
    if(a.gpuName)
      find.gpuName = a.gpuName.charAt(0).toUpperCase() + a.gpuName.slice(1);

    if(a.gpuMemorySize)
      valueChecker.push(calculateResult(a.gpuMemorySize, 'gpuMemorySize'));
    
    if(a.gpuMemoryType)
      find.gpuMemoryType = a.gpuMemoryType

    if(a.opticalDrive !== null)
      find.opticalDrive = a.opticalDrive




    valueChecker.forEach( (item, index) =>
      find = Object.assign(find, item)
    )

    const sort = {[`${body.formData.category}Rating`]: -1};
    if(body.formData.battery)
      sort.batteryLife = -1;

    let fullResponse = {laptops: []};

    let response = await Laptop.find(find)
    .populate('cpuModel')
    .populate('displayType', 'name')
    .populate('memoryType', 'name')
    .populate('primaryStorageModel', 'name')
    .populate('secondaryStorageModel', 'name')
    .populate('gpuModel', 'name')
    .sort(sort)
    .exec();

    response.map((element) => fullResponse.laptops.push(element));

    res.status(200).json(fullResponse);

  })()
});

router.get('/api/formula', (req, res) => {
  (async function(){

    let fullResponse = (await Formulas.find({}).exec())[0];
    res.status(200).json(fullResponse);

  })()
})

router.post('/api/formula-json', ({body}, res) => {
  console.log(body);
  fs.writeFile('../client/public/formula.json', JSON.stringify(body.data), function(err) {
    if(err) {
        return console.log(err);
    }
    console.log("The file was saved!");
}); 
})


const schema = new mongo.Schema({name: String, value: String});

const Categories = mongo.model('Categories', schema, 'categories')
const StorageSizes = mongo.model('StorageSizes', schema, 'storagesizes')
const ScreenSizes = mongo.model('ScreenSizes', schema, 'screensizes')

const CpuProducers = mongo.model('CpuProducers', schema, 'cpuproducers')
const CpuCores = mongo.model('CpuCores', schema, 'cpucores')
const BaseSpeed = mongo.model('BaseSpeed', schema, 'basecpuspeed')
const BoostSpeed = mongo.model('BoostSpeed', schema, 'boostcpuspeed')

const MemoryRatings = mongo.model('MemoryRatings', schema, "memoryratings");
const MemorySizes = mongo.model('MemorySizes', schema, 'memorysizes');

const DisplayRatings = mongo.model('DisplayRatings', schema, "displayratings");
const DisplayHorizontalResolutions = mongo.model('DisplayHorizontalResolutions', schema, "displayhorizontalresolutions")
const DisplayVerticalResolutions = mongo.model('DisplayVerticalResolutions', schema, "displayverticalresolutions")

const StorageRatings = mongo.model('StorageRatings', schema, "storageratings");

const GpuProducers =  mongo.model('GpuProducers', schema, "gpuproducers");
const GpuSizes =  mongo.model('GpuSizes', schema, "gpusizes");
const GpuTypes = mongo.model('GpuType', schema, "gpumemorytypes");

const Formulas = mongo.model('Formulas', schema, 'formulas');