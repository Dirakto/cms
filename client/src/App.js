import React, { useState, useCallback } from 'react';
import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom';
import './App.css';
import i18next from './i18n';
import { withTranslation } from 'react-i18next';
import SwitchButton from "react-switch";
import { IoMdMoon, IoMdSunny } from "react-icons/io";

import LaptopForm from './components/LaptopForm';
import AdminPanel from './components/AdminPanel';
import 'semantic-ui-css/semantic.min.css'
import Flag from "react-flags"

import ThemeActions from './ThemeAction';

function App(props) {
  const [lang, setLang] = useState(i18next.language);
  const [checked, setChecked] = useState(false);

  const changeLanguage = useCallback( (newLang) => {
    if(lang !== newLang){
      setLang(newLang);
      i18next.changeLanguage(newLang);
    }
  }, [lang]);

  const {t} = props;

  return (
    <Router>
      
      <Switch>
          <Route path="/admin" render={(props) => <AdminPanel {...props} />} />

          <Route path="/" render={() => (
            <div className={checked ? "dark_mode" : "light_mode"}>

              <div style={{display: 'flex', flexDirection: 'row'}}>
                <Link to="/">
                <div style={{margin: '10px 0 0 5px'}}>
                  <Flag
                    name="PL"
                    format="svg"
                    width={48}
                    height={32}
                    shiny={false}
                    alt={t("Flaga polska")}
                    basePath={process.env.PUBLIC_URL}
                    
                  />
                </div>
                </Link>
                <Link to="/en">
                <div style={{margin: '10px 0 0 5px'}}>
                  <Flag
                    style={{margin: '10px 5px 0 10px'}}
                    name="GB"
                    format="svg"
                    width={48}
                    height={32}
                    shiny={false}
                    alt={t("Flaga brytyjska")}
                    basePath={process.env.PUBLIC_URL}
                  />
                </div>
                </Link>
              </div>

              <img className="logo" src={process.env.PUBLIC_URL + "logo.png"} style={{width: '280px', height: '315px'}} alt={t("Logo")} />

              <div style={{position: 'absolute', right: '1rem', top: '0.5rem'}}>
                <SwitchButton
                  onChange={()=> {ThemeActions.changeTheme(!checked); setChecked(!checked) } }
                  checkedIcon={<IoMdMoon style={{marginLeft: '8px'}} />}
                  uncheckedIcon={<IoMdSunny style={{marginLeft: '6px', color: 'white'}} />}
                  onColor="#007bff"
                  checked={checked} />
              </div>

              <LaptopForm changeLanguage={changeLanguage}></LaptopForm>
            
            </div>
            )} />
      </Switch>
    </Router>
  );
}

export default withTranslation()(App);
