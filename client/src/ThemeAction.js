import Dispatcher from './Dispatcher';

class ThemeActions {
    changeTheme(){ 
       Dispatcher.dispatch({
           actionType: 'CHANGE_THEME'
       });
    }
}

export default new ThemeActions();