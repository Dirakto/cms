import Dispatcher from './Dispatcher';
import {EventEmitter} from 'events';

let lightMode = true;

class ThemeStore extends EventEmitter{
    constructor(){
        super();
        this.dispatchToken = Dispatcher.register(this.dispatcherCallback.bind(this));
    }

    emitChange(eventName){
        this.emit(eventName);
    }

    changeTheme(){
        lightMode = !lightMode;
    }

    getMode(){
        return lightMode;
    }

    addChangeListener(eventName, callback){
        this.on(eventName, callback);
    }
    removeChangeListener(eventName, callback) {
        this.removeListener(eventName, callback);
    }

    dispatcherCallback(action){
        switch(action.actionType){
            case 'CHANGE_THEME': {
                this.changeTheme();
                break;
            }
            default: {}
        }
        this.emitChange('STORE_'+action.actionType);

        return true;
    }

}

export default new ThemeStore();