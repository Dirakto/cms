import React, {useState, useEffect} from 'react';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';

import axios from 'axios';
import { URL, ADVANCED_FORM_DATA } from '../constants.js';
import ThemeStore from '../ThemeStore';
import { withTranslation } from 'react-i18next';

import { Accordion, Icon } from 'semantic-ui-react'
import '../styles/Accordion.css';

export default withTranslation()(function AdvancedAccordion(props){
    const [open, setOpen] = useState(0);
    const [openTabs, setOpenTabs] = useState([]);

    const {t} = props;

    const [optionSet, setOptionSet] = useState({
        cpuProducers: [],
        cpuCores: [],
        baseSpeed: [],
        boostSpeed: [],
        memoryTypes: [],
        memorySizes: [],
        horizontalResolutions: [],
        verticalResolutions: [],
        displayTypes: [],
        storageTypes: [],
        storageSizes: [],
        gpuNames: [],
        gpuMemorySizes: [],
        gpuMemoryTypes: [],
        opticalDrives: [{name: t("Tak"), value: true}, {name: t("Nie"), value: false}]
    });
    const [themeMode, setThemeMode] = useState("");

    let handleClick = (e, titleProps) => {
        const {index} = titleProps;
        setOpen(index === open ? 0 : index);
    }

    let handleClickTab = (e, titleProps) => {
        const {index} = titleProps;
        const tmp = [...openTabs];
        const positionOfIndex = tmp.indexOf(index);
        if(positionOfIndex < 0)
            tmp.push(index)
        else{
            tmp.splice(positionOfIndex, 1);
        }
        setOpenTabs(tmp);
    }

    const changeTheme = () => {
        if(!ThemeStore.getMode()){
            setThemeMode('accordion_dark_mode');
        }else{
            setThemeMode("");
        }
    }

    useEffect(()=> {
        ThemeStore.addChangeListener('STORE_CHANGE_THEME', changeTheme);
        axios.get(`${URL}advanced-form-data`)
        .then(({data}) => {
            // console.log(data)
            let oldObj = {...optionSet};
            if(data.cpuProducers.length > 0)
                oldObj.cpuProducers = data.cpuProducers;
            if(data.cpuCores.length > 0)
                oldObj.cpuCores = data.cpuCores;
            if(data.baseSpeed.length > 0)
                oldObj.baseSpeed = data.baseSpeed;
            if(data.boostSpeed.length > 0)
                oldObj.boostSpeed = data.boostSpeed;

            if(data.memoryTypes.length > 0)
                oldObj.memoryTypes = data.memoryTypes;
            if(data.memorySizes.length > 0)
                oldObj.memorySizes = data.memorySizes;

            if(data.horizontalResolutions.length > 0)
                oldObj.horizontalResolutions = data.horizontalResolutions;
            if(data.verticalResolutions.length)
                oldObj.verticalResolutions = data.verticalResolutions;
            if(data.displayTypes.length > 0)
                oldObj.displayTypes = data.displayTypes

            if(data.storageTypes.length > 0)
                oldObj.storageTypes = data.storageTypes
            if(data.storageSizes.length > 0)
                oldObj.storageSizes = data.storageSizes
            
            if(data.gpuNames.length > 0)
                oldObj.gpuNames = data.gpuNames
            if(data.gpuMemorySizes.length > 0)
                oldObj.gpuMemorySizes = data.gpuMemorySizes
            if(data.gpuMemoryTypes.length > 0)
                oldObj.gpuMemoryTypes = data.gpuMemoryTypes
        
            
            setOptionSet(oldObj)
        })
    }, [])



    return(
        <Accordion
            exclusive={false}
            styled
            className={themeMode+" advanced_accordion"}>
            <Accordion.Title
                active={open === 1}
                index={1}
                onClick={handleClick}
            >
                <label>{t("Advanced")}</label>
                <Icon name='dropdown' />
            </Accordion.Title>
            <Accordion.Content active={open === 1}>

                <Accordion
                    exclusive={false}
                    styled
                    className={themeMode+" advanced_accordion"}>
                    
                    <Accordion.Title
                        active={openTabs.includes(1)}
                        index={1}
                        onClick={handleClickTab}
                    >
                        <label>{t("Procesor")}</label>
                        <Icon name='dropdown' />
                    </Accordion.Title>
                    <Accordion.Content active={openTabs.includes(1)}>
                        <Container>
                            <Col>
                                <Option name="cpuProducer" label={t("Producent procesora")} value={optionSet.cpuProducers} {...props} />
                                <Option name="cpuCores" label={t("Ilość rdzeni")} value={optionSet.cpuCores} {...props} />
                                <Option name="cpuBaseSpeed" label={t("Bazowa częstotliwość taktowania")} value={optionSet.baseSpeed} {...props} />
                                <Option name="cpuBoostSpeed" label={t("Częstotliwość taktowania turbo")} value={optionSet.boostSpeed} {...props} />
                            </Col>
                        </Container>
                    </Accordion.Content>

                    <Accordion.Title
                        active={openTabs.includes(2)}
                        index={2}
                        onClick={handleClickTab}
                    >
                        <label>RAM</label>
                        <Icon name='dropdown' />
                    </Accordion.Title>
                    <Accordion.Content active={openTabs.includes(2)}>
                        <Container>
                            <Col>
                                <Option name="memoryTypes" label={t("Typ pamięci RAM")} value={optionSet.memoryTypes} {...props} />
                                <Option name="memorySizes" label={t("Wielkość pamięci RAM")} value={optionSet.memorySizes} {...props} />
                            </Col>
                        </Container>
                    </Accordion.Content>

                    <Accordion.Title
                        active={openTabs.includes(3)}
                        index={3}
                        onClick={handleClickTab}
                    >
                        <label>{t("Wyświetlacz")}</label>
                        <Icon name='dropdown' />
                    </Accordion.Title>
                    <Accordion.Content active={openTabs.includes(3)}>
                        <Container>
                            <Col>
                                <Option name="horizontalResolution" label={t("Szerokość")} value={optionSet.horizontalResolutions} {...props} />
                                <Option name="verticalResolution" label={t("Wysokość")} value={optionSet.verticalResolutions} {...props} />
                                <Option name="displayType" label={t("Typ wyświetlacza")} value={optionSet.displayTypes} {...props} />
                            </Col>
                        </Container>
                    </Accordion.Content>

                    <Accordion.Title
                        active={openTabs.includes(4)}
                        index={4}
                        onClick={handleClickTab}
                    >
                        <label>{t("Pamięć")}</label>
                        <Icon name='dropdown' />
                    </Accordion.Title>
                    <Accordion.Content active={openTabs.includes(4)}>
                        <Container>
                            <Col>
                                <Option name="storageType" label={t("Rodzaj pamięci")} value={optionSet.storageTypes} {...props} />
                                {/* <Option name="storageSize" label="Wielkość pamięci" value={optionSet.storageSizes} {...props} /> */}
                                <Option name="secondaryStorageType" label={t("Rodzaj dodatkowej pamięci")} value={optionSet.storageTypes} {...props} />
                                <Option name="secondaryStorageSize" label={t("Wielkość dodatkowej pamięci")} value={optionSet.storageSizes} {...props} />
                            </Col>
                        </Container>
                    </Accordion.Content>

                    <Accordion.Title
                        active={openTabs.includes(5)}
                        index={5}
                        onClick={handleClickTab}
                    >
                        <label>{t("Grafika")}</label>
                        <Icon name='dropdown' />
                    </Accordion.Title>
                    <Accordion.Content active={openTabs.includes(5)}>
                        <Container>
                            <Col>
                                <Option name="gpuName" label={t("Producent")} value={optionSet.gpuNames} {...props} />
                                <Option name="gpuMemorySize" label={t("Wielkość pamięci")} value={optionSet.gpuMemorySizes} {...props} />
                                <Option name="gpuMemoryType" label={t("Typ pamięci")} value={optionSet.gpuMemoryTypes} {...props} />
                            </Col>
                        </Container>
                    </Accordion.Content>

                    <Accordion.Title
                        active={openTabs.includes(6)}
                        index={6}
                        onClick={handleClickTab}
                    >
                        <label>{t("Inne")}</label>
                        <Icon name='dropdown' />
                    </Accordion.Title>
                    <Accordion.Content active={openTabs.includes(6)}>
                        <Container>
                            <Col>
                                <Option name="opticalDrive" label={t("Napęd optyczny")} value={optionSet.opticalDrives} {...props} />
                            </Col>
                        </Container>
                    </Accordion.Content>

                </Accordion>
            </Accordion.Content>
        </Accordion>
    );
})



const Option = (props) => {
    const [isDisabled, setEnabled] = useState(true);
    const [currentVal, changeCurrentVal] = useState(0);
    const {t, updateFormDataField} = props;

    const updateDataOnEnabled = () => {
        const tmp = isDisabled;
        setEnabled(!tmp);
        updateFormDataField(!tmp ? null : props.value[currentVal].value, ADVANCED_FORM_DATA, props.name);
    }

    const updateDataOnClicked = (e) => {
        updateFormDataField(e.target.value, ADVANCED_FORM_DATA, props.name)
        const tmp = props.value.filter( (item) => item.value === e.target.value);
        changeCurrentVal(props.value.indexOf(tmp[0]))
    }

    const options = props.value ? 
         props.value.map((option, index) => 
        <option key={index} value={option.value}>{option.name}</option> )
        : null

    return (
        <Form.Group as={Row}>
            <Form.Check
                inline
                type="checkbox"
                label=""
                name={props.name}
                onChange={ updateDataOnEnabled }
            />
            <Form.Label>{t(props.label)}</Form.Label>
            <Form.Control as="select" disabled={isDisabled} onChange={ updateDataOnClicked }>
                {options}
            </Form.Control>
        </Form.Group>
    );

}