import React, { useState, useEffect, Component } from 'react';
import {Route, Redirect, Switch} from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import FormulaEditorForm from './FormulaEditorForm'
import '../styles/FormulaEditorForm.css'
import '../styles/AdminPanel.css';

const AdminPanel = (props) => {
    const [isLogged, setLogged] = useState(false);

    const url = props.match.url
    useEffect(() => {
        console.log("XD")
        setLogged(false)
    }, []);

    return (
            <Switch>
                <PrivateRoute exact authed={isLogged} path={`${url}/editor`} component={FormulaEditor} />
                <Route exact path={`${url}`} render={() => <Login logIn={setLogged} url={url} history={props.history} />} />
            </Switch>
    );
}

function PrivateRoute({component: Component, authed, ...rest}){
    return(
      <Route
        {...rest}
        render={(props) => authed === true
          ? (<Component {...props} />)
          : (<Redirect to={{pathname: '/admin', state: {from: props.location}}} />)
        }
      />
    );
  }

const Login = (props) => {

    const handleSubmit = (e) => {
        e.preventDefault();
        props.logIn(true);
        props.history.push(`${props.url}/editor`)
    }

    useEffect(() => {
        props.logIn(false)
    }, []);

    return(
        <Container className="centered">
        <Col xs={{span: 6, offset: 3}}>

            <Form onSubmit={handleSubmit}>
                <Form.Group as={Row} >
                    <Form.Label>Login</Form.Label>
                    <Form.Control type="email" placeholder="Enter login" />
                </Form.Group>
                <Form.Group as={Row}>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Enter password" />
                </Form.Group>
                <Button style={{marginLeft: 'auto', marginRight: 'auto', display: 'block'}} type="submit">Zaloguj</Button>
            </Form>

        </Col>
        </Container>
    );

}

class FormulaEditor extends Component {
    constructor(props) {
        super(props);

        this.state = { formChosen: 'gaming', componentToRender: '' }
    }

    handleSubmit = e => {
        e.preventDefault();

        switch (this.state.formChosen) {
            case 'gaming': {
                this.setState({ componentToRender: <FormulaEditorForm formType="Gaming"></FormulaEditorForm> })
                break;
            }

            case 'business': {
                this.setState({ componentToRender: <FormulaEditorForm formType="Business"></FormulaEditorForm> })
                break;
            }

            case 'casual': {
                this.setState({ componentToRender: <FormulaEditorForm formType="Casual"></FormulaEditorForm> })
                break;
            }

            case 'contentcreating': {
                this.setState({ componentToRender: <FormulaEditorForm formType="Content Creation"></FormulaEditorForm> })
                break;
            }
        }

    };

    render() {
        if (this.state.componentToRender) return this.state.componentToRender
        else
            return (
                <div className="wrapper" >
                    <div className="form-wrapper">
                        <h1>Wybierz kategorię, dla której chcesz edytować formułę</h1><br></br>
                        <form onSubmit={this.handleSubmit} noValidate>
                            <div className="formDropdown">
                                <select value={this.state.formChosen} onChange={e => {
                                    this.setState({ formChosen: e.target.value })

                                }}>
                                    <option value="gaming">Gaming</option>
                                    <option value="business">Business Workstation</option>
                                    <option value="casual">Casual</option>
                                    <option value="contentcreating">Content Creation</option>
                                </select>
                            </div>
                            <div className="editForm">
                                <button type="submit">Edytuj formułę</button>
                            </div>
                        </form>
                    </div>
                </div>
            )
    }
}


export  default  AdminPanel;