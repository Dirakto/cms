import React, { Component } from "react";
import "../styles/FormulaEditorForm.css";
import axios from 'axios'
import {URL} from '../constants';

const formValid = ({ formErrors, ...rest }) => {
    let valid = true;

    // czy błędy formularza są puste
    Object.values(formErrors).forEach(val => {
        val.length > 0 && (valid = false);
    });

    // czy formularz został wypełniony
    Object.values(rest).forEach(val => {
        val === null && (valid = false);
    });

    return valid;
};

class FormulaEditor extends Component {
    constructor(props) {
        super(props);

        this.state = {
            batteryLife: 0,
            cpuModel: 0,
            displaySize: 0,
            displayType: 0,
            memorySize: 0,
            memorySpeed: 0,
            memoryType: 0,
            gpuModel: 0,
            formErrors: {
                batteryLife: "",
                cpuModel: "",
                displaySize: "",
                displayType: "",
                memorySize: "",
                memorySpeed: "",
                memoryType: "",
                gpuModel: ""
            },
            jsonFile: {}
        };
    }

    componentDidMount() {
        // const file = fs.readFileSync('../../helpers/formula.json', { encoding: 'utf-8' });
        axios.get('../formula.json').then(({ data }) => {
            this.state.jsonFile = data;

            const formulaObject = data[this.props.formType.toLowerCase()];

            const { batteryLifeWeight,
                cpuModelWeight,
                displaySizeWeight,
                displayTypeWeight,
                memorySizeWeight,
                memorySpeedWeight,
                memoryTypeWeight,
                gpuModelWeight,
                primaryStorageWeight } = formulaObject

            this.setState({
                batteryLife: batteryLifeWeight,
                cpuModel: cpuModelWeight,
                displaySize: displaySizeWeight,
                displayType: displayTypeWeight,
                memorySize: memorySizeWeight,
                memorySpeed: memorySpeedWeight, memoryType: memoryTypeWeight, gpuModel: gpuModelWeight, primaryStorage: primaryStorageWeight
            })

            console.log('Setting initial state')
        })

    }

    handleSubmit = e => {
        e.preventDefault();

        if (formValid(this.state)) {
            const oldJson = JSON.parse(JSON.stringify(this.state.jsonFile));

            oldJson[this.props.formType.toLowerCase()].batteryLifeWeight = this.state.batteryLife;
            oldJson[this.props.formType.toLowerCase()].cpuModelWeight = this.state.cpuModel;
            oldJson[this.props.formType.toLowerCase()].displaySizeWeight = this.state.displaySize;

            oldJson[this.props.formType.toLowerCase()].displayTypeWeight = this.state.displayType;

            oldJson[this.props.formType.toLowerCase()].memorySizeWeight = this.state.memorySize;

            oldJson[this.props.formType.toLowerCase()].memorySpeedWeight = this.state.memorySpeed;

            oldJson[this.props.formType.toLowerCase()].memoryTypeWeight = this.state.memoryType;

            oldJson[this.props.formType.toLowerCase()].gpuModelWeight = this.state.gpuModel;

            oldJson[this.props.formType.toLowerCase()].primaryStorageWeight = this.state.primaryStorage;

            console.log(oldJson);

            axios.post(`${URL}formula-json`, { data: oldJson })


            //         console.log(`
            //     --SUBMITTING--
            //     Battery Life weight: ${this.state.batteryLife}
            //     CPU weight: ${this.state.cpuModel}
            //     Display Size weight: ${this.state.displaySize}
            //     Display Type weight: ${this.state.displayType}
            //     Memory Size weight: ${this.state.memorySize}
            //     Memory Speed weight: ${this.state.memorySpeed}
            //     Memory Type weight: ${this.state.memoryType}
            //     GPU weight: ${this.state.gpuModel}
            //   `);
        } else {
            console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
        }
    };

    handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        let formErrors = { ...this.state.formErrors };

        switch (name) {
            case "batteryLife":
                formErrors.batteryLife =
                    value < 0 || value > 2 ? "waga musi być liczbą z przedziału od 0 do 2" : "";
                break;
            case "cpuModel":
                formErrors.cpuModel =
                    value < 0 || value > 2 ? "waga musi być liczbą z przedziału od 0 do 2" : "";
                break;
            case "displaySize":
                formErrors.displaySize =
                    value < 0 || value > 2 ? "waga musi być liczbą z przedziału od 0 do 2" : "";
                break;
            case "displayType":
                formErrors.displayType =
                    value < 0 || value > 2 ? "waga musi być liczbą z przedziału od 0 do 2" : "";
                break;
            case "memorySize":
                formErrors.memorySize =
                    value < 0 || value > 2 ? "waga musi być liczbą z przedziału od 0 do 2" : "";
                break;
            case "memorySpeed":
                formErrors.memorySpeed =
                    value < 0 || value > 2 ? "waga musi być liczbą z przedziału od 0 do 2" : "";
                break;
            case "memoryType":
                formErrors.memoryType =
                    value < 0 || value > 2 ? "waga musi być liczbą z przedziału od 0 do 2" : "";
                break;
            case "gpuModel":
                formErrors.gpuModel =
                    value < 0 || value > 2 ? "waga musi być liczbą z przedziału od 0 do 2" : "";
                break;
            default:
                break;
        }

        this.setState({ formErrors, [name]: value }, () => console.log(this.state));

    };



    render() {
        console.log('render')
        const { formErrors } = this.state;

        return (
            <div className="wrapper">
                <div className="form-wrapper">
                    <h1 className="h1_class">{this.props.formType} rating formula </h1>
                    <form onSubmit={this.handleSubmit} noValidate className="form_class">
                        <div className="batteryLife">
                            <label className="label_class" htmlFor="batteryLife">Battery Life weight</label>
                            <input
                                className={"input_class " + (formErrors.batteryLife.length > 0 ? "error" : null)}
                                placeholder="Wpisz wagę od 0 do 2"
                                type="number"
                                step="0.1"
                                name="batteryLife"
                                value={this.state.batteryLife}
                                noValidate
                                onChange={this.handleChange}
                            />
                            {formErrors.batteryLife.length > 0 && (
                                <span className="errorMessage">{formErrors.batteryLife}</span>
                            )}
                        </div>
                        <div className="cpuModel">
                            <label className="label_class" htmlFor="cpuModel">CPU weight</label>
                            <input
                                className={"input_class " + (formErrors.cpuModel.length > 0 ? "error" : null)}
                                placeholder="Wpisz wagę od 0 do 2"
                                type="number"
                                step="0.1"
                                name="cpuModel"
                                value={this.state.cpuModel}
                                noValidate
                                onChange={this.handleChange}
                            />
                            {formErrors.cpuModel.length > 0 && (
                                <span className="errorMessage">{formErrors.cpuModel}</span>
                            )}
                        </div>
                        <div className="displaySize">
                            <label className="label_class" htmlFor="displaySize">Display Size weight</label>
                            <input
                                className={"input_class " + (formErrors.displaySize.length > 0 ? "error" : null)}
                                placeholder="Wpisz wagę od 0 do 2"
                                type="number"
                                step="0.1"
                                name="displaySize"
                                noValidate
                                value={this.state.displaySize}
                                onChange={this.handleChange}
                            />
                            {formErrors.displaySize.length > 0 && (
                                <span className="errorMessage">{formErrors.displaySize}</span>
                            )}
                        </div>
                        <div className="displayType">
                            <label className="label_class" htmlFor="displayType">Display Type weight</label>
                            <input
                                className={"input_class " + (formErrors.displayType.length > 0 ? "error" : null)}
                                placeholder="Wpisz wagę od 0 do 2"
                                type="number"
                                step="0.1"
                                name="displayType"
                                noValidate
                                value={this.state.displayType}
                                onChange={this.handleChange}
                            />
                            {formErrors.displayType.length > 0 && (
                                <span className="errorMessage">{formErrors.displayType}</span>
                            )}
                        </div>
                        <div className="memorySize">
                            <label className="label_class" htmlFor="memorySize">Memory Size weight</label>
                            <input
                                className={"input_class " + (formErrors.memorySize.length > 0 ? "error" : null)}
                                placeholder="Wpisz wagę od 0 do 2"
                                type="number"
                                step="0.1"
                                name="memorySize"
                                noValidate
                                value={this.state.memorySize}
                                onChange={this.handleChange}
                            />
                            {formErrors.memorySize.length > 0 && (
                                <span className="errorMessage">{formErrors.memorySize}</span>
                            )}
                        </div>
                        <div className="memorySpeed">
                            <label className="label_class" htmlFor="memorySpeed">Memory Speed weight</label>
                            <input
                                className={"input_class " + (formErrors.memorySpeed.length > 0 ? "error" : null)}
                                placeholder="Wpisz wagę od 0 do 2"
                                type="number"
                                step="0.1"
                                name="memorySpeed"
                                value={this.state.memorySpeed}
                                noValidate
                                onChange={this.handleChange}
                            />
                            {formErrors.memorySpeed.length > 0 && (
                                <span className="errorMessage">{formErrors.memorySpeed}</span>
                            )}
                        </div>
                        <div className="memoryType">
                            <label className="label_class" htmlFor="memoryType">Memory Type weight</label>
                            <input
                                className={"input_class " + (formErrors.memoryType.length > 0 ? "error" : null)}
                                placeholder="Wpisz wagę od 0 do 2"
                                type="number"
                                step="0.1"
                                name="memoryType"
                                value={this.state.memoryType}
                                noValidate
                                onChange={this.handleChange}
                            />
                            {formErrors.memoryType.length > 0 && (
                                <span className="errorMessage">{formErrors.memoryType}</span>
                            )}
                        </div>
                        <div className="gpuModel">
                            <label className="label_class" htmlFor="gpuModel">GPU weight</label>
                            <input
                                className={"input_class " + (formErrors.gpuModel.length > 0 ? "error" : null)}
                                placeholder="Wpisz wagę od 0 do 2"
                                type="number"
                                step="0.1"
                                value={this.state.gpuModel}
                                name="gpuModel"
                                noValidate
                                onChange={this.handleChange}
                            />
                            {formErrors.gpuModel.length > 0 && (
                                <span className="errorMessage">{formErrors.gpuModel}</span>
                            )}
                        </div>
                        <div className="updateFormula">
                            <button type="submit">Zaktualizuj Wzór</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default FormulaEditor;
