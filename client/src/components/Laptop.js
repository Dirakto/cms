import React, {useState, useEffect} from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ListGroup from 'react-bootstrap/ListGroup';
import Modal from 'react-bootstrap/Modal';
import Carousel from 'react-bootstrap/Carousel';
import { withTranslation } from 'react-i18next';

import ThemeStore from '../ThemeStore';

import './../styles/Laptop.css';

const Laptop = withTranslation()((props) => {

    const [modalShow, setModalShow] = useState(false);

    let l = props.laptop;

    const {t} = props;

    let category = "";
    switch(props.currentCategory){
        case "business": category = `${t("Business Rating")}: ${Math.round(l.businessRating)}`; break;
        case "gaming": category = `${t("Gaming Rating")}: ${Math.round(l.gamingRating)}`; break;
        case "casual": category = `${t("Casual Rating")}: ${Math.round(l.casualRating)}`; break;
        case "contentcreating": category = `${t("Content Creating Rating")}: ${Math.round(l.contentcreatingRating)}`; break;
    }


    return (
        <ListGroup.Item className={props.nightMode} onClick={()=> {console.log("open"); setModalShow(true);}}>
        <Col>
            <Row style={{justifyContent: 'center'}}>
                <label style={{fontWeight: 'bold'}}>{l.laptopName}</label>
            </Row>
            <Row>
                <Col>
                    <img src={l.image_1} alt="Zdjecie laptopa" style={{width: '150px', height: '120px'}} />
                </Col>
                <Col>
                    <Row>
                    {t("Procesor")}: {l.cpuModel.name}
                    </Row>
                    <Row>
                    RAM: {l.memoryType.name}
                    </Row>
                    <Row>
                    {t("Grafika")}: {l.gpuModel.name}
                    </Row>
                    <Row>
                    {t("Matryca")}: {l.displayType.name}
                    </Row>

                </Col>
            </Row>

            <Row>
                <Col style={{fontSize: '1.4rem'}}>
                    {category}
                </Col>
                <Col style={{fontSize: '1.4rem', textAlign: 'right'}}>
                    {t("Cena")}: {l.prize}zł
                </Col>
            </Row>

        </Col>

        <LaptopInfo laptop={l} show={modalShow} onHide={() => setModalShow(false)} nightMode={props.nightMode} currentCategory={category} />
        </ListGroup.Item>

    );
})


const LaptopInfo = withTranslation()((props) => {

    let l = props.laptop;

    let secondaryStorage = null;
    if(l.secondaryStorageCap === 0){
        secondaryStorage = l.secondaryStorageModel.name;
    }else{
        secondaryStorage = l.secondaryStorageCap + " GB " + l.secondaryStorageModel.name
    }

    let style = {};
    if(props.nightMode){
        style = {
            backgroundColor: '#35363A',
            color: 'rgb(226, 225, 225) '
        };
    }

    let borderBottom = {borderBottom: null}
    if(props.nightMode){
        borderBottom.borderBottom = '1px dashed rgb(226, 225, 225)'
    }else{
        borderBottom.borderBottom = '1px dashed black'
    }

    const {t} = props;

    return (
        <div onClick={e => e.stopPropagation()}>
        <Modal show={props.show} onHide={props.onHide} centered size="xl" style={{height: '500px !important'}}>
            <Modal.Header closeButton  style={style} className={(props.nightMode ? 'my_close' : null)}>
                <Modal.Title>{l.laptopName}</Modal.Title>
            </Modal.Header>
            <Modal.Body  style={style}>
                <Col>
                    <Row>
                        <Col xs={5}>
                        <div style={{display: 'flex', flexDirection: 'column'}}>
                            <div style={borderBottom}>{t("Procesor")}</div>
                            <div>{l.cpuProducer +" "+l.cpuModel.name}</div>
                            <div>{t("Liczba rdzeni")+": "+l.cpuCores}</div>
                            <div>{t("Bazowa częstotliwość taktowania")+": "+l.cpuBaseSpeed} GHz</div>
                            <div>{t("Częstotliwość taktowania turbo")+": "+l.cpuBoostSpeed} GHz</div>
                        
                            <div className="item-padding" style={borderBottom}>{t("Wyświetlacz")}</div>
                            <div>{l.displaySize + "\" " + l.displayType.name}</div>
                            <div>{t("Wymiary")+": "+l.horizontalResolution + "x" + l.verticalResolution +" p."}</div>
       
                            <div className="item-padding" style={borderBottom}>RAM</div>
                            <div>{l.memorySize + " GB " + l.memoryType.name}</div>
                            <div>{t("Prędkość")+": "+l.memorySpeed + " MHz"}</div>
                      
                            <div className="item-padding" style={borderBottom}>{t("Pamięć")}</div>
                            <div>{t("Pamięć wewnętrzna")+": "+l.primaryStorageCap + " GB " +l.primaryStorageModel.name}</div>
                            <div>{t("Dodatkowa pamięć")+": "+secondaryStorage}</div>
                            
                            <div className="item-padding" style={borderBottom}>{t("Karta graficzna")}</div>
                            <div>{l.gpuName + " " + l.gpuModel.name}</div>
                            <div>{t("Typ pamięci")+": "+l.gpuMemoryType.toUpperCase()}</div>
                            <div>{t("Prędkość")+": "+l.gpuMemorySize + " MHz "}</div>
                           
                            <div className="item-padding" style={borderBottom}>{t("Pozostałe")}</div>
                            <div>{t("Karta sieciowa")+": "+l.wirelessCard}</div>
                            <div>{t("Napęd optyczny")+": "+(l.opticalDrive ? t("Tak") : t("Brak")) }</div>
                            <div>{t("Czas pracy baterii")+": "+l.batteryLife+ "h"}</div>
                      
                        </div>
                        </Col>
                        <Col xs={7} style={{alignSelf: 'center', textAlign: 'center'}}>
                            <Carousel className="visibility_small">
                                <Carousel.Item>
                                    <img src={l.image_1} style={{width: '20vw', height: '30vh', maxWidth: '250px', maxHeight: '215px', minWidth: '240px', minHeight: '210px'}} className="d-block" alt="Laptop 1" />
                                </Carousel.Item>
                                <Carousel.Item>
                                    <img src={l.image_2} style={{width: '20vw', height: '30vh', maxWidth: '250px', maxHeight: '215px', minWidth: '240px', minHeight: '210px'}} className="d-block" alt="Laptop 2" />
                                </Carousel.Item>
                            </Carousel>

                            <img src={l.image_1} style={{width: '20vw', height: '30vh', maxWidth: '250px', maxHeight: '215px', minWidth: '240px', minHeight: '210px'}} className="hidden_small" alt="Laptop 1" /> 
                            <img src={l.image_2} style={{width: '20vw', height: '30vh', maxWidth: '260px', maxHeight: '225px', minWidth: '250px', minHeight: '220px', paddingLeft: '10px'}} className="hidden_small" alt="Laptop 2" />
                            
                        </Col>
                    </Row>
                </Col>
                
            </Modal.Body>
            <Modal.Footer  style={style}>
                <Col style={{fontSize: '1.7rem'}}>
                    {props.currentCategory}
                </Col>
                <Col style={{fontSize: '1.7rem', textAlign: 'right'}}>
                    {t("Cena")}: {l.prize}zł
                </Col>
            </Modal.Footer>
        </Modal>
        </div>
    );
})


const LaptopGroup = (props) => {

    const [themeMode, setThemeMode] = useState("");

    const changeTheme = () => {
        if(!ThemeStore.getMode()){
            setThemeMode("list-group-item_dark_mode")
        }else{
            setThemeMode("");
        }
    };

    useEffect(() => {
        ThemeStore.addChangeListener('STORE_CHANGE_THEME', changeTheme);
    }, []);

    if(!props.laptops)
        return null;


    let laptopSet = props.laptops.map((element, index) =>
        <Laptop key={index} laptop={element} nightMode={themeMode} currentCategory={props.currentCategory} />
    )

    return (
        <ListGroup className="slider" style={{position: 'absolute', height: '100%', bottom: '20px', width: '100%'}} > 
            {laptopSet}
        </ListGroup>
    );
}

export default LaptopGroup;