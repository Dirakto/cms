import React, {Component} from 'react';
import axios from 'axios';
import { URL, FORM_DATA } from '../constants.js';
import {Route} from 'react-router-dom';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import LaptopGroup from './Laptop';
import AdvancdedAccordion from './Accordion';

import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';

import { withTranslation } from 'react-i18next';

import "../styles/LaptopForm.css";


class LaptopForm extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            sliderValue: 0,
            categoryOptions: [],
            storageSizes: [],
            screenSizes: [],
            formData: {
                category: '',
                battery: null,
                weight: null,
                screenSize: '',
                storageSize: ''
            },
            advancedFormData: {
                cpuProducer: null,
                cpuCores: null,
                cpuBaseSpeed: null,
                cpuBoostSpeed: null,
                memoryTypes: null,
                memorySizes: null,
                horizontalResolution: null,
                verticalResolution: null,
                displayType: null,
                storageType: null,
                secondaryStorageType: null,
                secondaryStorageSize: null,
                gpuName: null,
                gpuMemorySize: null,
                gpuMemoryType: null,
                opticalDrive: null
            },
            currentCategory: null,
            response: null,
        }
        this.advancedCheckboxes = [];
        for(let i = 20; i >= 0; i--)
            this.advancedCheckboxes.push(React.createRef());

        this.handleSubmit = this.handleSubmit.bind(this);
        this.updateFormDataField = this.updateFormDataField.bind(this);
    }

    componentDidMount(){
        axios.get(`${URL}basic-form-data`)
            .then(({data}) => {
                let oldObj = {...this.state.formData};
                if(data.categories.length > 0)
                    oldObj.category = data.categories[0].value;
                if(data.storageSizes.length > 0)
                    oldObj.storageSize = data.storageSizes[0].value;
                if(data.screenSizes.length > 0)
                    oldObj.screenSize = data.screenSizes[0].value;
                this.setState({categoryOptions: data.categories, storageSizes: data.storageSizes, screenSizes: data.screenSizes, formData: oldObj});
            }); 
    }

    updateFormDataField(value, stateField, innerFieldName){
        let oldObj = {...this.state[stateField]};
        oldObj[innerFieldName] = value;
        this.setState({[stateField]: oldObj})
    }


    handleSubmit(event){
        event.preventDefault();
        // console.log(this.state.formData)
        // console.log(this.state.advancedFormData)
        axios.post(`${URL}laptops`, {
            prize: this.state.sliderValue,
            formData: this.state.formData,
            advancedFormData: this.state.advancedFormData
        }).then(
            ({data}) => {
                // console.log(data.laptops)
                this.setState({response: data.laptops});
            }
        )
        this.setState({currentCategory: this.state.formData.category});
    }

    render(){
        const {t} = this.props;

        let categoryOptions = this.state.categoryOptions.map((option, index) => 
            <option key={index} value={option.value}>{option.name}</option> )
        let storageSizesOptions = this.state.storageSizes.map((option, index) => 
            <option key={index} value={option.value}>{option.name}</option> )
        let screenSizesOptions = this.state.screenSizes.map((option, index) => 
            <option key={index} value={option.value}>{option.name}</option> )

        return(
            <Container className="main_container">
                <div className="banner"><label className="title">{t("Jakiego laptopa szukasz?")}</label></div>
                <Row>
                    <Col xs={5}>
                        <Form onSubmit={this.handleSubmit}>
                        <Form.Group as={Row}>
                            <Form.Label as={Col} xs={12}>{t("Do czego będziesz używał laptopa?")}</Form.Label>
                            <Col xs={12}>
                                <Form.Control as="select" value={this.state.formData.category} onChange={(e) => this.updateFormDataField(e.target.value, FORM_DATA, 'category')}>{categoryOptions}</Form.Control>
                            </Col>
                        </Form.Group>

                        <Form.Group as={Row}>
                            <Form.Label as={Col} xs={12}>{t("Budżet")}</Form.Label>
                            <Col xs={12} className="input_range_column" >
                                <InputRange
                                    draggableTrack={true}
                                    maxValue={10000}
                                    minValue={0}
                                    step={10}
                                    value={this.state.sliderValue}
                                    onChange={value => this.setState({sliderValue: value}) }
                                />
                            </Col>
                        </Form.Group>

                        <Form.Group as={Row}>
                            <Form.Label as={Col} xs={12}>{t("Jak ważny jest długi czas pracy na baterii?")}</Form.Label>
                            <Col xs={12}>
                                <Form.Check
                                    inline
                                    type="radio"
                                    label={t("Mało ważny")}
                                    name="battery"
                                    value="low"
                                    id="batteryOpt1"
                                    onChange={ (e) => this.updateFormDataField(e.target.value,  FORM_DATA, 'battery')}
                                />
                                {/* <Form.Check
                                    inline
                                    type="radio"
                                    label="dość ważny"
                                    name="battery"
                                    value="medium"
                                    id="batteryOpt2"
                                    onChange={ (e) => this.updateFormDataField(e.target.value, FORM_DATA, 'battery')}
                                /> */}
                                <Form.Check
                                    inline
                                    type="radio"
                                    label={t("Bardzo ważny")}
                                    name="battery"
                                    value="high"
                                    id="batteryOpt3"
                                    onChange={ (e) => this.updateFormDataField(e.target.value, FORM_DATA, 'battery')}
                                />
                            </Col>
                        </Form.Group>

                        <Form.Group as={Row}>
                            <Form.Label as={Col} xs={12}>{t("Czy mała waga jest ważna?")}</Form.Label>
                            <Col xs={12}>
                                <Form.Check
                                    inline
                                    type="radio"
                                    label={t("Tak")}
                                    name="weight"
                                    value={true}
                                    id="weightOpt1"
                                    onChange={ (e) => this.updateFormDataField(e.target.value, FORM_DATA, 'weight')}
                                />
                                <Form.Check
                                    inline
                                    type="radio"
                                    label={t("Nie")}
                                    name="weight"
                                    value={false}
                                    id="weightOpt2"
                                    onChange={ (e) => this.updateFormDataField(e.target.value, FORM_DATA, 'weight')}
                                />
                            </Col>
                        </Form.Group>

                        <Form.Group as={Row}>
                            <Form.Label as={Col}>{t("Jak duży powinien być ekran?")}</Form.Label>
                            <Col xs={12}>
                                <Form.Control as="select" value={this.state.formData.screenSize} onChange={ (e) => this.updateFormDataField(e.target.value, FORM_DATA, 'screenSize')}>
                                    {screenSizesOptions}
                                </Form.Control>
                            </Col>
                        </Form.Group>

                        <Form.Group as={Row}>
                        <Form.Label as={Col}>{t("Jaka pojemność dysku?")}</Form.Label>
                            <Col xs={12}>
                                <Form.Control as="select" value={this.state.formData.storageSize} onChange={ (e) => this.updateFormDataField(e.target.value, FORM_DATA, 'storageSize')}>
                                    {storageSizesOptions}
                                </Form.Control>
                            </Col>
                        </Form.Group>

                        <Row>
                        <AdvancdedAccordion t={t} updateFormDataField={this.updateFormDataField}>
                        </AdvancdedAccordion>
                        </Row>

                            <Row>
                                <Button className="submit_button" variant="primary" type="submit">{t("Wyślij")}</Button>
                            </Row>
                        </Form>
                    </Col>

                    <Col style={{marginLeft: '40px'}}>
                        <LaptopGroup laptops={this.state.response} currentCategory={this.state.currentCategory} />
                    </Col>

                </Row>

                <Route exact path="/" render={ (props) => this.props.changeLanguage("pl")} />
                <Route exact path="/en" render={ (props) => this.props.changeLanguage("en")} />
                
            </Container>
        );
    }

}


export default withTranslation()(LaptopForm);