const mongo = require('mongoose');

/**
 *  Basic schema for ratings
 * 
 *  @author Oskar Rajzer
 */
const schema = mongo.Schema({
    name: String,
    rating: Number
});
mongo.model('CpuRating', schema);
mongo.model('DisplayRating', schema);
mongo.model('MemoryRating', schema);
mongo.model('StorageRating', schema);
mongo.model('GpuRating', schema);

/**
 *  Laptop schema consistent with Keystone.js (keystone/index.js) & MongoDB.
 *  Extends fields not related to keystone.js
 * 
 *  @author Oskar Rajzer
 */
const LaptopSchema = mongo.Schema({
    prize: Number,
    gamingRating: Number,
    businessRating: Number,
    casualRating: Number,
    contentcreatingRating: Number,
    laptopName: String,
    image_1: String,
    image_2: String,
    wirelessCard: String,
    opticalDrive: Boolean,
    batteryLife: String,
    cpuProducer: String,
    cpuModel: { type: mongo.Schema.Types.ObjectId, ref: 'CpuRating' },
    // cpuModel: String,
    cpuCores: Number,
    cpuBaseSpeed: Number,
    cpuBoostSpeed: Number,
    displaySize: Number,
    horizontalResolution: Number,
    verticalResolution: Number,
    displayType: { type: mongo.Schema.Types.ObjectId, ref: 'DisplayRating' },
    // displayType: String,
    memorySize: Number,
    memorySpeed: Number,
    memoryType: { type: mongo.Schema.Types.ObjectId, ref: 'MemoryRating' },
    // memoryType: String,
    primaryStorageModel: { type: mongo.Schema.Types.ObjectId, ref: 'StorageRating' },
    // primaryStorageModel: String,
    primaryStorageCap: Number,
    secondaryStorageModel: { type: mongo.Schema.Types.ObjectId, ref: 'StorageRating' },
    // secondaryStorageModel: String,
    secondaryStorageCap: Number,
    gpuName: String,
    gpuModel: { type: mongo.Schema.Types.ObjectId, ref: 'GpuRating' },
    // gpuModel: String,
    gpuMemorySize: Number,
    gpuMemoryType: String
});

module.exports = mongo.model('Laptops', LaptopSchema, 'laptops');