const math = require('mathjs');
const fs = require('fs');
const mongo = require('mongoose');
const Laptop = require('./LaptopSchema');

// const equations = JSON.parse(fs.readFileSync('./equation.json', "utf-8"));
const equations = JSON.parse(fs.readFileSync('../helpers/formula.json', "utf-8"));

let db;

module.exports = async function calculate_rating(laptop_id){
    mongo.connect('mongodb://localhost:27017/cms-project', { useNewUrlParser: true, useUnifiedTopology: true });
    db = mongo.connection;
    db.on('error', console.error.bind(console, 'connection error:'));

    db.once('open', () => {
        let foundLaptop = Laptop.find({})
        .populate('cpuModel', 'rating')
        .populate('displayType', 'rating')
        .populate('memoryType', 'rating')
        .populate('primaryStorageModel', 'rating')
        .populate('secondaryStorageModel', 'rating')
        .populate('gpuModel', 'rating')
        .exec((err, laptop) => {
            if(err){
                console.log(err);
                db.close();
                return;
            }
            laptop.forEach( (element) => {
                console.log(element)
                calculateRate(element)
            }
            )
        })
    });

    async function calculateRate(laptop){
        let battery = laptop.batteryLife.split(':');
        const scope = {
            batteryLife: battery[0]+battery[1]/60,
            cpuModel: laptop.cpuModel.rating,
            displaySize: laptop.displaySize,
            displayType: laptop.displayType.rating,
            memorySize: laptop.memorySize,
            memorySpeed: laptop.memorySpeed,
            memoryType: laptop.memoryType.rating,
            primaryStorage: laptop.primaryStorageModel.rating,
            primaryStorageCap: laptop.primaryStorageCap,
            secondaryStorage: laptop.secondaryStorageModel.rating,
            secondaryStorageCap: !laptop.secondaryStorageCap ? 0 : laptop.secondaryStorageCap,
            gpuModel: laptop.gpuModel.rating
        }
        // console.log(scope);
        let resultGaming = math.evaluate(equations.gaming, scope);
        let resultBusiness = math.evaluate(equations.business, scope);
        let resultCasual = math.evaluate(equations.casual, scope);
        let resultContentcreating = math.evaluate(equations.contentcreating, scope);
        // console.log(resultGaming);
        // console.log(resultBusiness);
        // console.log(resultCasual);
        // console.log(resultContentcreating);
        Laptop.updateOne({_id: laptop._id}, {
            gamingRating: resultGaming,
            businessRating: resultBusiness,
            casualRating: resultCasual,
            contentcreatingRating: resultContentcreating
        }, (err, affected, resp) => {
            if(err)
                console.log(err);
            if(affected)
                console.log("Values changed");
            if(resp)
                console.log(resp);
        }).then(() => db.close())
    }

}







// const math = require('mathjs');
// const fs = require('fs');
// const mongo = require('mongoose');
// const Laptop = require('./LaptopSchema');

// const equations = JSON.parse(fs.readFileSync('./equation.json', "utf-8"));
// console.log(equations);

// mongo.connect('mongodb://localhost:27017/cms-project', { useNewUrlParser: true, useUnifiedTopology: true });
// var db = mongo.connection;
// db.on('error', console.error.bind(console, 'connection error:'));

// db.once('open', () => {
//     let foundLaptop = Laptop.findOne({laptopName: "Dell Inspiron 13 5379 2-in-1"})
//     .populate('cpuModel', 'rating')
//     .populate('displayType', 'rating')
//     .populate('memoryType', 'rating')
//     .populate('primaryStorageModel', 'rating')
//     .populate('secondaryStorageModel', 'rating')
//     .populate('gpuModel', 'rating')
//     .exec((err, laptop) => {
//         if(err){
//             console.log(err);
//             db.close();
//             return;
//         }

//         calculateRate(laptop);

//         // db.close();
//     })
// });

// function calculateRate(laptop){
//     let battery = laptop.batteryLife.split(':');
//     const scope = {
//         batteryLife: battery[0]+battery[1]/60,
//         cpuModel: laptop.cpuModel.rating,
//         displaySize: laptop.displaySize,
//         displayType: laptop.displayType.rating,
//         memorySize: laptop.memorySize,
//         memorySpeed: laptop.memorySpeed,
//         memoryType: laptop.memoryType.rating,
//         primaryStorage: laptop.primaryStorageModel.rating,
//         primaryStorageCap: laptop.primaryStorageCap,
//         secondaryStorage: laptop.secondaryStorageModel.rating,
//         secondaryStorageCap: !laptop.secondaryStorageCap ? 0 : laptop.secondaryStorageCap,
//         gpuModel: laptop.gpuModel.rating
//     }
//     console.log(scope);
//     let resultGaming = math.evaluate(equations.gaming, scope);
//     let resultBusiness = math.evaluate(equations.business, scope);
//     let resultCasual = math.evaluate(equations.casual, scope);
//     let resultContentcreating = math.evaluate(equations.contentcreating, scope);
//     console.log(resultGaming);
//     console.log(resultBusiness);
//     console.log(resultCasual);
//     console.log(resultContentcreating);
//     Laptop.updateOne({_id: laptop._id}, {
//         gamingRating: resultGaming,
//         businessRating: resultBusiness,
//         casualRating: resultCasual,
//         contentcreatingRating: resultContentcreating
//     }, (err, affected, resp) => {
//         if(err)
//             console.log(err);
//         if(affected)
//             console.log("Values changed");
//         if(resp)
//             console.log(resp);
//     }).then(() => db.close())
// }