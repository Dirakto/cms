'use strict'
const mongo = require('mongoose');
const readLaptop = require('./laptop_reader');
const prompt = require('prompt');

let prompt_attr = [
    {
        name: 'category',
        validator: /[A-Za-z\s_\d]+/
    },
    {
        name: 'part'
    },
    {
        name: 'value'
    }
];

prompt.start();
prompt.get(prompt_attr, (err, result) => {
    if(err){
        console.log(err);
        return 1;
    }else{
        let allValues = readLaptop(result.part, result.value);
        const schema = mongo.Schema({
            name: String,
            rating: Number
        });
        
        schema.pre('save', async function(next){
            let exists = await NewModel.exists({name: this.name});
            if(!exists)
                next();
            else{
                console.log('Exists');
                next(new Error("User exists!"))
            }
            
        })
        let NewModel = mongo.model(result.category, schema);
        

        let schemaObjects = allValues.map((el) => {
            return new NewModel({name: el, rating: undefined});
            
        });

        mongo.connect('mongodb://localhost:27017/cms-project', { useNewUrlParser: true, useUnifiedTopology: true });
        var db = mongo.connection;
        db.on('error', console.error.bind(console, 'connection error:'));

        db.once('open', function(){
            let promises = [];
            schemaObjects.forEach((el) => {
                promises.push( new Promise(resolve => 
                    el.save(function(err, schemaObj){
                        if(err) console.log(err);
                        else console.log(el.name+" added.");
                        resolve();
                    })
                ));
            });
            Promise.all(promises).then(() => db.close());
        });
    }
});