'use-strict'

const fs = require('fs');
const req = require('request-promise');

const numberOfLaptops = 50;

/**
 *  Allows to download N number of laptops from noteb.com site
 *  synchronously (because laptops ids aren't sequential)
 * 
 *  @author Oskar Rajzer
 */
async function doStuff(){
    let i = 1175;
    let counter = 0
    while(counter < numberOfLaptops){
        let result = await createLaptop(i);
        if( result ){
            counter++;
        }
        i++;
    }
}

async function createLaptop(i){
    let answer = false;
    await req.post('https://noteb.com/api/webservice.php', {
        form: {
            apikey: '112233aabbcc',
            method: 'get_model_info',
            'param[model_id]': i
        }
    }).then( function ( body) {
        console.log(JSON.parse(body).code)
        if (JSON.parse(body).code == 26) {
            fs.writeFile(`laptops/laptop_${i}.json`, body, (err) => {
            if (err)
                answer = false;
                console.log(err);
            });
            answer = true;
        }
    });
    return answer;
}

doStuff();