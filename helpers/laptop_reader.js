'use-strict'

const fs = require('fs');

/**
 * Finds all values for specified laptop part and value
 * @param {string} part Part to look for
 * @param {string} [value] Value to look for
 * @author Oskar Rajzer
 */
module.exports = function(part, value){
    const allValues = new Set();
    fs.readdirSync('R:\\JsNode\\CMS\\helpers\\laptops').forEach(file => {
        let tmp = fs.readFileSync(`laptops/${file}`)
        if(!!value || typeof value ==='string')
            allValues.add(JSON.parse(tmp).result['0'][part][value])
        else
            allValues.add(JSON.parse(tmp).result['0'][part])
    });

    return Array.from(allValues).sort();
}