const fs = require('fs');
const mongo = require('mongoose');
const Laptop = require('./LaptopSchema');

/**
 *  Loads all JSON files from 'helpers/laptops' and saves them
 *  in MongoDB with fields consistent with Keystone.js (keystone/index.js)
 * 
 *  @author Oskar Rajzer
 */

const laptops = [];

(function loadLaptops(){
    fs.readdirSync('R:\\JsNode\\CMS\\helpers\\laptops').forEach(file => {
        let tmp = JSON.parse(fs.readFileSync(`laptops/${file}`)).result['0'];
        let newItem = new Laptop({
            prize: null,
            category: null,
            laptopName: tmp['model_info'][0]['name'],
            image_1: tmp['model_resources']['image_1'],
            image_2: tmp['model_resources']['image_2'],
            wirelessCard: tmp['wireless_card']['model'],
            opticalDrive: tmp['optical_drive']['type'] === 'none' ? false : true,
            batteryLife: tmp['battery_life_hours'],
            cpuProducer: tmp['cpu']['prod'],
            cpuModel: tmp['cpu']['model'],
            cpuCores: tmp['cpu']['cores'],
            cpuBaseSpeed: tmp['cpu']['base_speed'],
            cpuBoostSpeed: tmp['cpu']['boost_speed'],
            displaySize: tmp['display']['size'],
            horizontalResolution: tmp['display']['horizontal_resolution'],
            verticalResolution: tmp['display']['vertical_resolution'],
            displayType: tmp['display']['type'].startsWith('LED TN') ? 'LTN' :'LIPS', 
            memorySize: tmp['memory']['size'],
            memorySpeed: tmp['memory']['speed'],
            memoryType: tmp['memory']['type'].toLowerCase(),
            primaryStorageModel: tmp['primary_storage']['model'],
            primaryStorageCap: tmp['primary_storage']['cap'],
            secondaryStorageModel: tmp['secondary_storage']['model'],
            secondaryStorageCap: tmp['secondary_storage']['cap'],
            gpuName: tmp['gpu']['prod'],
            gpuModel: tmp['gpu']['model'],
            gpuMemorySize: tmp['gpu']['memory_size'],
            gpuMemoryType: tmp['gpu']['memory_type'].toLowerCase()
        });
        laptops.push( newItem );
    });
})();


mongo.connect('mongodb://localhost:27017/cms-project', { useNewUrlParser: true, useUnifiedTopology: true });
var db = mongo.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function(){
    console.log("Connection Successful!");

    // var item = new Laptop({
    //     prize: 23233.32,
    //     category: "Gaming",
    //     laptopName: "Dell Inspiron 15 7577 Gaming",
    //     image_1: "https://noteb.com/res/img/models/970_1.jpg",
    //     image_2: "https://noteb.com/res/img/models/970_2.jpg",
    //     wirelessCard: "802.11 ac",
    //     opticalDrive: true,
    //     batteryLife: "4:20",
    //     cpuProd: "Intel",
    //     cpuModel: "i5-7300HQ",
    //     cores: 4,
    //     baseSpeed: 1.60,
    //     boostSpeed: 3.40,
    //     displaySize: 15.6,
    //     horizontalRes: 1920,
    //     verticalRes: 1080,
    //     displayType: "LIPS",
    //     memorySize: 8,
    //     memorySpeed: 2400,
    //     memoryType: 'DDR4',
    //     modelPrimary: 'SSD',
    //     capPrimary: 256,
    //     modelSecondary: 'HDD',
    //     capSecondary: 256,
    //     gpuName: "Nvidia",
    //     gpuModel: "GeForce GTX 1060 Max-Q",
    //     gpuMemorySize: 6144,
    //     gpuMemoryType: 'GDDR5'
    // });

    const promises = [];

    laptops.forEach((item) => {
        promises.push( new Promise(resolve =>
            item.save(function(err, book){
                if(err) return console.error(err);
                console.log(item.laptopName+" added.");
                resolve();
            })
        ))
    });

    var results = Promise.all(promises);
           
    results.then(() => {
        db.close();
    });
        
})