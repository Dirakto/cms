const { Keystone } = require('@keystonejs/keystone');
const { PasswordAuthStrategy } = require('@keystonejs/auth-password');
const { Text, Password, Checkbox, Relationship, Select, Integer, Float} = require('@keystonejs/fields');
const { Markdown } = require('@keystonejs/fields-markdown');
const { GraphQLApp } = require('@keystonejs/app-graphql');
const { AdminUIApp } = require('@keystonejs/app-admin-ui');
const { StaticApp } = require('@keystonejs/app-static');

const calculate_rating = require('../helpers/calculate_ratings');

/* keystone-cli: generated-code */
const { MongooseAdapter: Adapter } = require('@keystonejs/adapter-mongoose');
const PROJECT_NAME = 'CMSProject';
/* /keystone-cli: generated-code */

const keystone = new Keystone({
  name: PROJECT_NAME,
  adapter: new Adapter(),
});

const displayOptions = [
  {value: 'LIPS', label: 'LED IPS'},
  {value: 'LTN', label: 'LED TN'},
];
const ramOptions = [
  {value: 'ddr3', label: 'DDR3'},
  {value: 'ddr4', label: 'DDR4'},
  {value: 'ddr5', label: 'DDR5'}
];
const categoryOptions = [
  {value: 'gaming', label: 'Gaming'},
  {value: 'business', label: 'Business Workstation'},
  {value: 'casual', label: 'Casual'},
  {value: 'contentcreating', label: 'Content Creating'}
];

/* Laptops */
keystone.createList('Laptop', {
  schemaDoc: 'Laptops for the project',
  fields: {
    /* App required data */
    prize: { type: Float },
    desc: { type: Markdown, allowHtmlTags: true },// NO FIELD DOCS
    /* Main data */
    laptopName: { type: Text, schemaDoc: 'Nazwa' },
    image_1: { type: Text },
    image_2: { type: Text },
    wirelessCard: { type: Text },
    opticalDrive: { type: Checkbox },
    batteryLife: { type: Text },
    /* CPU */
    cpuProducer: { type: Text },
    cpuModel: { type: Relationship, ref: 'CpuRating' },
    // cpuModel: { type: Text },
    cpuCores: { type: Integer },
    cpuBaseSpeed: { type: Float },
    cpuBoostSpeed: { type: Float },
    /* Display */
    displaySize: { type: Float },
    horizontalResolution: { type: Integer },
    verticalResolution: { type: Integer },
    displayType: { type: Relationship, ref: 'DisplayRating' },
    // displayType: { type: Select, options: displayOptions},
    /* Memory */
    memorySize: { type: Integer },
    memorySpeed: { type: Integer },
    memoryType: { type: Relationship, ref: 'MemoryRating' },
    // memoryType: { type: Select, options: ramOptions},
    /* Storage */
    primaryStorageModel: { type: Relationship, ref: 'StorageRating' },
    // primaryStorageModel: { type: Text },
    primaryStorageCap: { type: Integer },
    secondaryStorageModel: { type: Relationship, ref: 'StorageRating' },
    // secondaryStorageModel: { type: Text },
    secondaryStorageCap: { type: Integer },
    /* GPU */
    gpuName: { type: Text },
    gpuModel: { type: Relationship, ref: 'GpuRating' },
    // gpuModel: { type: Text },
    gpuMemorySize: { type: Integer },
    gpuMemoryType: { type: Text }
  },
  labelField: 'laptopName',
  hooks: {
    afterChange: async ({
      operation,
      existingItem,
      originalInput,
      updatedItem,
      context,
      actions,
    }) => {
      calculate_rating(updatedItem.id);
    }
  }
});

/* CpuRating */
keystone.createList('CpuRating', {
  schemaDoc: 'Ratings for CPUs',
  fields: {
    name: { type: Text },
    rating: { type: Float }
  }
});
/* DisplayRating */
keystone.createList('DisplayRating', {
  schemaDoc: 'Ratings for Displays',
  fields: {
    name: { type: Text },
    rating: { type:Float }
  }
});
/* MemoryRating */
keystone.createList('MemoryRating', {
  schemaDoc: 'Ratings for Memories',
  fields: {
    name: { type: Text },
    rating: { type:Float }
  }
});
/* StorageRating */
keystone.createList('StorageRating', {
  schemaDoc: 'Ratings for Storages',
  fields: {
    name: { type: Text },
    rating: { type:Float }
  }
});
/* GpuRating */
keystone.createList('GpuRating', {
  schemaDoc: 'Ratings for GPUs',
  fields: {
    name: { type: Text },
    rating: { type:Float }
  }
});

/* Categories */
keystone.createList('Categorie', {
  schemaDoc: 'Categories of laptops',
  fields: {
    name: { type: Text},
    value: { type: Text}
  },
  labelField: 'value'
})
keystone.createList('ScreenSize', {
  schemaDoc: 'Proposed screen sizes',
  fields: {
    name: { type: Text },
    value: { type: Text }
  },
  labelField: 'value'
})
keystone.createList('StorageSize', {
  schemaDoc: 'Proposed storage sizes',
  fields: {
    name: { type: Text },
    value: { type: Text }
  },
  labelField: 'value'
})


// ==================================================================== //

/* Access controll */
const userIsAdmin = ({ authentication: { item: user } }) => Boolean(user && user.isAdmin);
const userOwnsItem = ({ authentication: { item: user } }) => {
  if (!user) {
    return false;
  }
  return { id: user.id };
};
const userIsAdminOrOwner = auth => {
  const isAdmin = access.userIsAdmin(auth);
  const isOwner = access.userOwnsItem(auth);
  return isAdmin ? isAdmin : isOwner;
};
const access = { userIsAdmin, userOwnsItem, userIsAdminOrOwner };

/* User Authentication */
keystone.createList('User', {
  fields: {
    name: { type: Text },
    email: {
      type: Text,
      isUnique: true,
    },
    isAdmin: { type: Checkbox },
    password: {
      type: Password,
    },
  },
  access: {
    read: access.userIsAdminOrOwner,
    update: access.userIsAdminOrOwner,
    create: access.userIsAdmin,
    delete: access.userIsAdmin,
    auth: true,
  },
});

const authStrategy = keystone.createAuthStrategy({
  type: PasswordAuthStrategy,
  list: 'User',
  config: {
    identityField: 'name',
    secretField: 'password',
  },
});

// app.post('/admin/signin', async (req, res) => {
//   const username = req.body.username;
//   const password = req.body.password;

//   const result = await this.authStrategy.validate({
//     identity: username,
//     secret: password,
//   });

//   if (result.success) {
//     // Create session and redirect
//     // ..
//   }

//   // Return the failure
//   return res.json({ success: false, message: result.message });
// });


module.exports = {
  keystone,
  apps: [
    new GraphQLApp(),
    new StaticApp({ path: '/', src: 'public' }),
    new AdminUIApp({ enableDefaultRoute: true, authStrategy }),
  ],
};
